'use strict';

angular.module('yoApp.admin', [
  'yoApp.auth',
  'ui.router'
]);

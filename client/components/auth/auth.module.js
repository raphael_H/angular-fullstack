'use strict';

angular.module('yoApp.auth', [
  'yoApp.constants',
  'yoApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
